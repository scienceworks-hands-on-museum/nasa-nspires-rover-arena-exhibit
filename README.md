# NASA NSPIRES Rover Arena Exhibit
An enclosed rover challenge course.

Features:
* IR remote joystick controls
* NASA-inspired rover model
* Real Mars terrain
* Modular obstacles
* Conceptual Mars habitat models from NASA design contest

## Design Files
[Sketchup design](https://app.sketchup.com/share/tc/northAmerica/oX7KKsrCbn0?stoken=-BuidgxHln19IbVqudvpXM-EUvL-KRWyIIZemmtdzOxC1GstdhKj8p82f3Qus98K&source=web)

[CNC, 3d printing, and other files](https://drive.google.com/drive/folders/1pqu6uNwzrVVidynh1F_1aBI1n2T6bkp-?usp=drive_link)

[Resource: NASA 3d models](https://nasa3d.arc.nasa.gov/models/printable)

## Photos
[Build Process Photos](https://photos.app.goo.gl/bSNaeZpCfFtUCyYX9)

## Documentation
[Setup Raspberry Pi Zero for 4Tronix Rover](https://4tronix.co.uk/blog/?p=2409)

NOTE: Dependencies are missing in above instructions. Also run `sudo pip install .`

[Python GPIO documentation](https://sourceforge.net/p/raspberry-gpio-python/wiki/Inputs/)

## Troubleshooting
`Can't open /dev/mem: Permission denied`: This error is related to the LEDs. You must be root, run the program with `sudo`. Unfortunately this means the install must be run with sudo.
