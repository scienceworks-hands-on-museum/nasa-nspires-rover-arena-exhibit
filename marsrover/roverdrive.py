# Mars Rover Simple Drive Mode
# Similar to motortest.py but integrates servo steering
# Moves: Forward, Reverse, turn Right, turn Left, Stop 
# Press Ctrl-C to stop

import rover, time

#======================================================================
# Reading single character by forcing stdin to raw mode
import sys
import tty
import termios


class RoverDrive:

    # Servo numbers
    servo_FL = 9
    servo_RL = 11
    servo_FR = 15
    servo_RR = 13
    servo_MA = 0
    speed = 60

    steerMin = -30
    steer = 0
    steerDiff = 15
    steerMax = 30

    @classmethod
    def goForward(cls):
        rover.forward(cls.speed)

    @classmethod
    def steerStraight(cls):
        rover.setServo(cls.servo_FL, 0)
        rover.setServo(cls.servo_FR, 0)
        rover.setServo(cls.servo_RL, 0)
        rover.setServo(cls.servo_RR, 0)
    
    @classmethod
    def goReverse(cls):
        rover.reverse(cls.speed)

    @classmethod
    def goLeft(cls):
        cls.steer += cls.steerDiff
        cls.steer = min(cls.steer, cls.steerMax)

        rover.setServo(cls.servo_FL, -cls.steer)
        rover.setServo(cls.servo_FR, -cls.steer)
        rover.setServo(cls.servo_RL, cls.steer)
        rover.setServo(cls.servo_RR, cls.steer)

    @classmethod
    def goRight(cls):
        cls.steer -= cls.steerDiff
        cls.steer = max(cls.steer, cls.steerMin)

        rover.setServo(cls.servo_FL, -cls.steer)
        rover.setServo(cls.servo_FR, -cls.steer)
        rover.setServo(cls.servo_RL, cls.steer)
        rover.setServo(cls.servo_RR, cls.steer)
