#!/usr/bin/env python

# Read lirc output, in order to sense key presses on an IR remote.
# There are various Python packages that claim to do this but
# they tend to require elaborate setup and I couldn't get any to work.
# This approach requires a lircd.conf but does not require a lircrc.
# If irw works, then in theory, this should too.
# Based on irw.c, https://github.com/aldebaran/lirc/blob/master/tools/irw.c

import socket

class Infrared:
    SOCKPATH = "/var/run/lirc/lircd"
    sock = None

    @classmethod
    def init(cls):
        cls.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        print ('starting up on %s' % cls.SOCKPATH)
        cls.sock.connect(cls.SOCKPATH)

    @classmethod
    def next(cls):
        '''
        Get the next key pressed. Return keyname, updown.
        '''
        while True:
            data = cls.sock.recv(128)
            #print("Data: " + data.decode())
            data = data.strip()
            if data:
                break

        words = data.split()
        return words[2], words[1]


