
from infrared import Infrared
import rover
from roverdrive import RoverDrive
from setInterval import setInterval
import time

class MarsRover:
    IR_CHANNEL = 24
    LED_BRIGHTNESS = 40
    
    _STOP_INTERVAL_SECONDS = 0.5
    _autonomous = False
    _lastButtonPressTime = 0        
    _interval = None

    @classmethod
    def init(cls):
        rover.init(cls.LED_BRIGHTNESS)

        rover.rainbow()
        Infrared.init()
        rover.show()

        RoverDrive.steerStraight()

        cls._interval = setInterval(cls._tryStop, cls._STOP_INTERVAL_SECONDS / 2)

    @classmethod
    def _tryStop(cls):
        if cls._autonomous: return

        diff = time.time() - cls._lastButtonPressTime
        
        if diff >= cls._STOP_INTERVAL_SECONDS:
            print('Stop')
            rover.stop()

    @classmethod
    def pilot(cls):
        button, updown = Infrared.next()
        button = button.decode('utf-8')
        updown = updown.decode('utf-8')
        cls._lastButtonPressTime = time.time() 

        print(button + ' ' + updown)

        if button == 'BTN_1':
            RoverDrive.goLeft()
        elif button == 'BTN_2':
            RoverDrive.goForward()
        elif button == 'BTN_3':
            RoverDrive.goRight()
        elif button == 'BTN_4':
            RoverDrive.goReverse()
        elif button == 'BTN_5':
            cls._autonomous = True

    @classmethod
    def cleanup(cls):
        if cls._interval: cls._interval.cancel()
        rover.cleanup()

def main():
    MarsRover.init()
    
    try:
        while True:
            MarsRover.pilot()
    except KeyboardInterrupt:
        pass
    finally:
        MarsRover.cleanup()

if __name__ == '__main__':
    main()
