/**
 * An IR remote controller for a rover.
 * Install library IRremote before compiling.
 */

#define IR_SEND_PIN 3


// #include "TinyIRSender.hpp"
#include <IRremote.hpp>
IRsend irsend;

#define LEFT_PIN 4
#define BACKWARD_PIN 5
#define RIGHT_PIN 6
#define FORWARD_PIN 7
#define AUTO_PIN 8
#define LED_PIN 9

#define ADDRESS 0x707
#define COMMAND_LEFT 0x60
#define COMMAND_RIGHT 0x61
#define COMMAND_FORWARD 0x62
#define COMMAND_BACKWARD 0x65
#define COMMAND_AUTO 0x68

#define AUTO_TIME_MS 30 * 1000

unsigned long t = millis();
bool autoMode = false;

void setup() {
  Serial.begin(9600);
  pinMode(LEFT_PIN, INPUT);
  pinMode(BACKWARD_PIN, INPUT);
  pinMode(RIGHT_PIN, INPUT);
  pinMode(FORWARD_PIN, INPUT);
  pinMode(AUTO_PIN, INPUT);
  pinMode(LED_PIN, OUTPUT);
}

void loop() {
  if (digitalRead(LEFT_PIN)) {
    irsend.sendSamsung(ADDRESS, COMMAND_LEFT, 2);
    Serial.println("Left");
  } else if (digitalRead(BACKWARD_PIN)) {
    irsend.sendSamsung(ADDRESS, COMMAND_BACKWARD, 2);
    Serial.println("Backward");
  } else if (digitalRead(RIGHT_PIN)) {
    irsend.sendSamsung(ADDRESS, COMMAND_RIGHT, 2);
    Serial.println("Right");
  } else if (digitalRead(FORWARD_PIN)) {
    irsend.sendSamsung(ADDRESS, COMMAND_FORWARD, 2);
    Serial.println("Forward");
  }

  if (digitalRead(AUTO_PIN)) {
    irsend.sendSamsung(ADDRESS, COMMAND_AUTO, 2);
    Serial.println("Auto");
    t = millis();
    autoMode = true;
  }

  if (millis() - t >= AUTO_TIME_MS) {
    autoMode = false;
  }
  
  if (autoMode) {
    digitalWrite(LED_PIN, HIGH);
  } else {
    digitalWrite(LED_PIN, LOW);
  }

  delay(100);
}