from setuptools import setup, find_packages

setup(
    name='swx_mars_rover',
    version='1.0',
    packages=find_packages(),
    dependencies=[
        'pca9685',
        'rpi_ws281x',
        'RPi.GPIO',
        'smbus',
    ],
)
